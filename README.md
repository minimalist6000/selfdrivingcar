# README #

A* (A Star) pathfinding algorithm implementation in Java.

Made for assignment in Artificial Intelligence course in Tartu University.

![2015-12-14 03.14.24 pm.png](https://bitbucket.org/repo/6qEppE/images/2209552164-2015-12-14%2003.14.24%20pm.png)

## Assignment Requirements ##
Program should read a text file of intersections and text file of roads that are connected via these intersections.
Then should ask user from where to where user wants to go and then should find quickly shortest path.

Path finding is been done via A* algorithm. 
 
To make it more visual - the intersections and roads are shown as 'Ascii map'.
First all possible paths (the whole known world for the program), later the shortest path.

### Full assignment details (in Estonian) ###
Auto-programm (5 punkti) peaks toimima linna kaartidel, mis on esitatud tippudest ja servadest koosnevate graafide kujul. Ristmikud antakse ette tekstifailina ristmikud.txt, mille igas reas on ristmiku nimi ja selle koordinaadid (eraldajaks tühik), nt:

```
#!

r1 2 1
r2 1 1
r3 0 1
r4 0 2
r5 0 0
r6 1 2
```

Tänavad kirjeldatakse failis teed.txt, mille igas reas on kahe ristmiku ja neid ühendava tänava nimed, nt:


```
#!

r1 r2 Liivi
r2 r3 Liivi
r4 r3 Veski
r3 r5 Veski 
r1 r6 Baeri
```

Programm loeb tänavate ja ristmike failid sisse ja küsib kasutajalt, kust kuhu soovitakse sõita. Kasuta võimalikult kiire tee leidmiseks heuristilist otsingualgoritmi (A-Star või IDA-Stat) või loogilist tuletusmeetodit (resolutsioon või Herbrandi meetod).

Lisaboonus (2 punkti) disjunktihulkadel põhineva loogilise tuletusmeetodi kasutamise eest.