package com.ut.ai;

public class UIAsciiMapRoad {

	public int startX;
	public int endX;
	public int startY;
	public int endY;
	public boolean isVertical;
	public boolean isHorizontal;
	public boolean isDiagonal;
	public Intersection upperIntersection;
	public Intersection lowerIntersection;

	public UIAsciiMapRoad(Road road) {
		this.startX = UIAsciiMap.scaleX(road.getStart().getX() < road.getEnd().getX() ? road.getStart().getX() : road.getEnd().getX());
		this.endX   = UIAsciiMap.scaleX(road.getStart().getX() < road.getEnd().getX() ? road.getEnd().getX()   : road.getStart().getX());				

		this.startY = UIAsciiMap.scaleY(road.getStart().getY() < road.getEnd().getY() ? road.getStart().getY() : road.getEnd().getY());
		this.endY   = UIAsciiMap.scaleY(road.getStart().getY() < road.getEnd().getY() ? road.getEnd().getY()   : road.getStart().getY());
		
		this.isVertical   = this.startX == this.endX;
		this.isHorizontal = this.startY == this.endY;
		this.isDiagonal   = !this.isHorizontal && !this.isVertical;
		
		this.upperIntersection = road.getStart();
		this.lowerIntersection = road.getEnd();		 
		if (this.upperIntersection.getY() > this.lowerIntersection.getY()){
			this.upperIntersection = road.getEnd();
			this.lowerIntersection = road.getStart();
		}
	}
}
