package com.ut.ai;

import java.util.ArrayList;

public class AStar {
	
	
	private Map map;

	public AStar(Map m) {
		this.map = m;
	}

	public ArrayList<Intersection> getShortestPath(){
		ArrayList<Intersection> openList   = new ArrayList<>();
		ArrayList<Intersection> closedList = new ArrayList<>();
	    ArrayList<Intersection> neighbours = new ArrayList<>();
	    
	    this.map.getStart().setGScore(0);
	    openList.add(this.map.getStart());
	    
	    while (openList.size() > 0){
	    	Intersection current = getIntersectctionWithLowestFCost(openList);
	    	
	    	if (current.equals(this.map.getGoal())) {
                return reconstructPath(current);
            }    
	    	
	    	openList.remove(current);
	    	closedList.add(current);
	    	
	    	neighbours = getValidNeighbours(current);
	    	
	    	for (Intersection neigbour : neighbours){
	    		Boolean isNeighbourBetter = false;
	    		
	    		if (closedList.contains(neigbour))
	    			continue;
	    		
	    		int neighborFullGScrore = current.getGScore() + neigbour.getGScoreTemp();
	    		
	    		if (!openList.contains(neigbour)){
	    			openList.add(neigbour);
	    			isNeighbourBetter = true;
	    		} else if(neighborFullGScrore < current.getGScore()) {
	    			isNeighbourBetter = true;
	    		}

		    	if (isNeighbourBetter){
		    		neigbour.setParent(current);
		    		neigbour.setGScore(neighborFullGScrore);
		    		neigbour.setHScore(this.map.getEstimateDistanceForIntersection(neigbour));
		    	}
	    	}	    	
	    }	    
		return null;
	}


	private Intersection getIntersectctionWithLowestFCost(ArrayList<Intersection> list) {
		Intersection lowestF = list.get(0);

		for (Intersection intersection : list) {
			if (intersection.getFScore() < lowestF.getFScore()){
				lowestF = intersection;
			}
		}		
        return lowestF;
	}
	
	

	private ArrayList<Intersection> getValidNeighbours(Intersection current) {
		ArrayList<Intersection> neighbours = new ArrayList<>();
        int x = current.getX();
        int y = current.getY();
        
        
        Intersection north     = this.map.getValidIntersectionByCoordinates(current, x  , y-1);	
		Intersection northEast = this.map.getValidIntersectionByCoordinates(current, x+1, y-1);
		Intersection east      = this.map.getValidIntersectionByCoordinates(current, x+1, y  );
		Intersection southEast = this.map.getValidIntersectionByCoordinates(current, x+1, y+1);
		Intersection south     = this.map.getValidIntersectionByCoordinates(current, x  , y+1);
		Intersection southWest = this.map.getValidIntersectionByCoordinates(current, x-1, y+1);
		Intersection west      = this.map.getValidIntersectionByCoordinates(current, x-1, y  );
		Intersection northWest = this.map.getValidIntersectionByCoordinates(current, x-1, y-1);
		
	    if (north != null){
	    	north.setGScoreTemp(10);
	    	neighbours.add(north);
	    }	 
	    if (northEast != null){
	    	northEast.setGScoreTemp(14);
	    	neighbours.add(northEast);
	    }	
	    if (east != null) {
	    	east.setGScoreTemp(10);
	    	neighbours.add(east);
	    }
	    if (southEast != null) {
	    	southEast.setGScoreTemp(14);
	    	neighbours.add(southEast);
	    }
	    if (south != null) {
	    	south.setGScoreTemp(10);
	    	neighbours.add(south);
	    }
	    if (southWest != null) {
	    	southWest.setGScoreTemp(14);
	    	neighbours.add(southWest);
	    }
	    if (west != null) {
	    	west.setGScoreTemp(10);
	    	neighbours.add(west);
	    }
	    if (northWest != null) {
	    	northWest.setGScoreTemp(14);
	    	neighbours.add(northWest);
	    }
	    
		return neighbours;
	}
	
	
	private ArrayList<Intersection> reconstructPath(Intersection current) {
		Intersection intersection = current;
        ArrayList<Intersection> path = new ArrayList<>();
        while (!(intersection.getParent() == null)) {
        	path.add(0, intersection.getParent());
            intersection = intersection.getParent();
        }
        path.add(current);
        return path;		
	}
}

