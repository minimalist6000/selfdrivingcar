package com.ut.ai;

public class Intersection {

	private String name;
	private int x;
	private int y;
	private int gScore;  
	private int gScoreTemp;  
	private int hScore;   
	private Intersection parent;
	
	public Intersection(String name, int x, int y){
		this.name = name;
		this.x = x;
		this.y = y;
		this.gScore     = Integer.MAX_VALUE;
		this.gScoreTemp = Integer.MAX_VALUE;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}
	
	public int getGScore() {
		return gScore;
	}

	public void setGScore(int gScore) {
		this.gScore = gScore;
	}

	public int getGScoreTemp() {
		return gScoreTemp;
	}

	public void setGScoreTemp(int gScoreTemp) {
		this.gScoreTemp = gScoreTemp;
	}

	public int getHScore() {
		return hScore;
	}

	public void setHScore(int hScore) {
		this.hScore = hScore;
	}

	public int getFScore() {
		return gScore + hScore;
	}

	public Intersection getParent() {
		return parent;
	}

	public void setParent(Intersection parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Intersection [name=" + name + ", x=" + x + ", y=" + y + "]";
	}

	
	
	
}
