package com.ut.ai;

import java.util.ArrayList;
import java.util.Scanner;

public class UI {
	
	private Map map;
	private boolean isAborted;
	Scanner sc;
	
	
	public UI(Map m) {
		this.map = m;
		this.isAborted = false;
		this.sc = new Scanner(System.in);
	}

	public void showIntroduction(){
		System.out.println("Self Driving Car / Pathfinding by Indrek Ilves");
		System.out.println("-----------------------------------------------");
		//System.out.println("Hello commuter.");
		//System.out.println("-----------------------------------------------");
	}
	
	
	public void showMap() {
		System.out.println("Map of known roads and intersections.");
		System.out.println("-----------------------------------------------");
		UIAsciiMap am = new UIAsciiMap(this.map.getIntersections(), this.map.getRoads());
		am.show();	
		System.out.println("-----------------------------------------------");
	}

	public boolean askStartAndGoal() {
		if (!this.isAborted) 
			askStart();
		if (!this.isAborted)
			askGoal();	
		if (this.isAborted){
			System.out.println("Navigation aborted.");
			return false;
		}	
		
		return true; 
	}

	private void askStart() {		
		Intersection start = null;
		start = askLocation("location (nearest intersection)");	
		this.map.setStart(start);
		RoadPoint startRP = this.map.findRoadPointByIntersect(start);
		this.map.setStartRoadPoint(startRP);   	
	}
	
	private void askGoal() {
		Intersection goal = null;
		goal = askLocation("destination");	
		this.map.setGoal(goal);
		RoadPoint goalRP = this.map.findRoadPointByIntersect(goal);
		this.map.setGoalRoadPoint(goalRP);
	}

	private Intersection askLocation(String locString) {
		String startStr = "";
		Intersection intersection = null;
		do{
			System.out.println("Please enter your " + locString + ":");
			System.out.println("Type 'Quit' to close the application.");			
			
			startStr = getLocationStr();			
			if (startStr.equals("QUIT")){
				this.isAborted = true;
				return null;
			}
			
			intersection = this.map.findIntersectionByName(startStr);
			if (intersection == null){
				System.out.println("This is invalid intersection.\n");
			}
		} while (intersection == null);
		return intersection;
	}

	private String getLocationStr() {
		String s = null;
		try {
			s = this.sc.next().trim().toUpperCase();				
		} catch (Exception e) {
			System.out.println("This is invalid intersection.\n");
			this.sc.next();
		}
		return s;
	}

	public void showShortestPath(ArrayList<Intersection> shortestPath) {
		if (shortestPath == null){
			System.out.println("Path from " + this.map.getStart().getName() + " to " + this.map.getGoal().getName() + " not found.");
			System.out.println("-----------------------------------------------");
		} else {		
			System.out.println("Map of shortest path.");
			System.out.println("-----------------------------------------------");
			String startStr = this.map.getStart().getName();
			String goalStr  = this.map.getGoal().getName();
			this.map.getStart().setName("From");
			this.map.getGoal().setName("To");
			
			ArrayList<Road> shortestPathRoads = this.map.getRoadsBetweenIntersections(shortestPath);
			UIAsciiMap am = new UIAsciiMap(shortestPath, shortestPathRoads);
			am.show();	

			this.map.getStart().setName(startStr);
			this.map.getGoal().setName(goalStr);			
			showShortestPathDescribtion(shortestPathRoads);
			
			System.out.println("-----------------------------------------------");
		}
	}

	private void showShortestPathDescribtion(ArrayList<Road> shortestPathRoads) {
		System.out.println();
		String s = "From intersection " + this.map.getStart().getName();
		Road prev = null;
		int cnt = 1;
		for (Road r : shortestPathRoads) {
			s = cnt + ". " + s + " " + getAction(prev, r) + " " + r.getName() + " st.";   
			System.out.println(s);
			s = "Then";
			prev = r;
			cnt++;
		}
		System.out.println(cnt + ". You have arrived at your destination - Intersection " + this.map.getGoal().getName());
		System.out.println();
	}

	private String getAction(Road prev, Road r) {
		String action = "take first exit to";
		if (prev != null && r.getName().equals(prev.getName()))
			action = "continue straight via";
		if (prev != null && !r.getName().equals(prev.getName()))
			action = "turn to";
		return action;
	}

	public boolean askDoYouWantToContinue() {
		if (this.isAborted) return false;
		System.out.println("Do you want to continue? (Y/N)");
		char c = 'N';
		try {
			c = this.sc.next().trim().toUpperCase().charAt(0);				
		} catch (Exception e) {
			this.sc.next();
		}
		return c == 'Y';
	}



}
