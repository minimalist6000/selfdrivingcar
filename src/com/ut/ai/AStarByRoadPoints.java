package com.ut.ai;

import java.util.ArrayList;

public class AStarByRoadPoints {

	private Map map;

	public AStarByRoadPoints(Map m) {
		this.map = m;
	}

	public ArrayList<RoadPoint> getShortestPath() {
		ArrayList<RoadPoint> openList   = new ArrayList<>();
		ArrayList<RoadPoint> closedList = new ArrayList<>();
	    ArrayList<RoadPoint> neighbours = new ArrayList<>();
	    
	    this.map.getStartRoadPoint().setGScore(0);
	    openList.add(this.map.getStartRoadPoint());
	    
	    while (openList.size() > 0){
	    	RoadPoint current = getRoadPointWithLowestFCost(openList);
	    	
	    	if (current.equals(this.map.getGoalRoadPoint())) {
                return reconstructPath(current);
            }    
	    	
	    	openList.remove(current);
	    	closedList.add(current);
	    	
	    	neighbours = getValidNeighbours(current);
	    	
	    	for (RoadPoint neigbour : neighbours){
	    		Boolean isNeighbourBetter = false;
	    		
	    		if (closedList.contains(neigbour))
	    			continue;
	    		
	    		int neighborFullGScrore = current.getGScore() + neigbour.getGScoreTemp();
	    		
	    		if (!openList.contains(neigbour)){
	    			openList.add(neigbour);
	    			isNeighbourBetter = true;
	    		} else if(neighborFullGScrore < current.getGScore()) {
	    			isNeighbourBetter = true;
	    		}

		    	if (isNeighbourBetter){
		    		neigbour.setParent(current);
		    		neigbour.setGScore(neighborFullGScrore);
		    		neigbour.setHScore(this.map.getEstimateDistanceForRoadPoint(neigbour));
		    	}
	    	}	    	
	    }	    
		return null;
	}



	private RoadPoint getRoadPointWithLowestFCost(ArrayList<RoadPoint> openList) {
		RoadPoint lowestF = openList.get(0);

		for (RoadPoint rp : openList) {
			if (rp.getFScore() < lowestF.getFScore()){
				lowestF = rp;
			}
		}		
        return lowestF;
	}
	
	

	private ArrayList<RoadPoint> getValidNeighbours(RoadPoint current) {
		ArrayList<RoadPoint> neighbours = new ArrayList<>();
        int x = current.getX();
        int y = current.getY();
        
        
        RoadPoint north     = this.map.getValidRoadPointByCoordinates(current, x  , y-1);	
        RoadPoint northEast = this.map.getValidRoadPointByCoordinates(current, x+1, y-1);
        RoadPoint east      = this.map.getValidRoadPointByCoordinates(current, x+1, y  );
        RoadPoint southEast = this.map.getValidRoadPointByCoordinates(current, x+1, y+1);
        RoadPoint south     = this.map.getValidRoadPointByCoordinates(current, x  , y+1);
        RoadPoint southWest = this.map.getValidRoadPointByCoordinates(current, x-1, y+1);
        RoadPoint west      = this.map.getValidRoadPointByCoordinates(current, x-1, y  );
		RoadPoint northWest = this.map.getValidRoadPointByCoordinates(current, x-1, y-1);
		
	    if (north != null){
	    	north.setGScoreTemp(10);
	    	neighbours.add(north);
	    }	 
	    if (northEast != null){
	    	northEast.setGScoreTemp(14);
	    	neighbours.add(northEast);
	    }	
	    if (east != null) {
	    	east.setGScoreTemp(10);
	    	neighbours.add(east);
	    }
	    if (southEast != null) {
	    	southEast.setGScoreTemp(14);
	    	neighbours.add(southEast);
	    }
	    if (south != null) {
	    	south.setGScoreTemp(10);
	    	neighbours.add(south);
	    }
	    if (southWest != null) {
	    	southWest.setGScoreTemp(14);
	    	neighbours.add(southWest);
	    }
	    if (west != null) {
	    	west.setGScoreTemp(10);
	    	neighbours.add(west);
	    }
	    if (northWest != null) {
	    	northWest.setGScoreTemp(14);
	    	neighbours.add(northWest);
	    }
	    
		return neighbours;
	}
	
	
	private ArrayList<RoadPoint> reconstructPath(RoadPoint current) {
		RoadPoint rp = current;
        ArrayList<RoadPoint> path = new ArrayList<>();
        while (!(rp.getParent() == null)) {
        	path.add(0, rp.getParent());
            rp = rp.getParent();
        }
        path.add(current);
        return path;		
	}
}
