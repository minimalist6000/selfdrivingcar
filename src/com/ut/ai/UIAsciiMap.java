package com.ut.ai;

import java.util.ArrayList;

public class UIAsciiMap {

	private ArrayList<Intersection> intersections;
	private ArrayList<Road> roads;
	private int rows;
	private int columns;
	private char[][] am;

	public UIAsciiMap(ArrayList<Intersection> intersections, ArrayList<Road> roads) {
		this.intersections = intersections;
		this.roads         = roads;
		this.rows          = getRows()    *  4 + 2;
		this.columns       = getColumns() * 10 + 8;
	    this.am            = new char[this.rows][this.columns];
	}

	public void show() {
		addIntersectionsToAsciiMap();  	
		addRoadsToAsciiMap();		
		drawAsciiMap();		
	}


	private int getRows() {
		int rows = 0;
		for (Intersection intersection : this.intersections) {
			if (intersection.getY() > rows)
				rows = intersection.getY();
		}
		return rows;
	}

	private int getColumns() {
		int columns = 0;
		for (Intersection intersection : this.intersections) {
			if (intersection.getX() > columns)
				columns = intersection.getX();
		}
		return columns;
	}

	private void addIntersectionsToAsciiMap() {
		for (Intersection intersection : this.intersections) {
			int x = scaleX(intersection.getX());
			int y = scaleY(intersection.getY());
		       
			for (char c : intersection.getName().toCharArray()){
				//System.out.println("y:" + y + " x:" + x + " c:" + c );
				this.am[y][x] = c;			    
				x++;
			}
		}
	}

	private void addRoadsToAsciiMap() {
		for (Road road : this.roads){
			UIAsciiMapRoad amRoad = new UIAsciiMapRoad(road);
			
			if (amRoad.isVertical) {
				 int y = amRoad.startY;
				 while (y <= amRoad.endY){
					 if (this.am[y][amRoad.startX] == 0)
						 this.am[y][amRoad.startX] = '|';
					 y++;
				 }
			}
			
			if (amRoad.isHorizontal) {
				 int x = amRoad.startX;
				 while (x <= amRoad.endX){
					 if (this.am[amRoad.startY][x] == 0)
						 this.am[amRoad.startY][x] = '-';
					 x++;
				 }
			}
			
			if (amRoad.isDiagonal){
				 // if upper intersection is on the left  - align vertical road to the left
				 // if upper intersection is on the right - align vertical road to the right
				 int y = amRoad.startY;
				 int x = amRoad.startX;
				 if (amRoad.upperIntersection.getX() > amRoad.lowerIntersection.getX())
					 x = amRoad.endX;
			
				 while (y <= amRoad.endY){
					 if (this.am[y][x] == 0)
						 this.am[y][x] = '|';
					 y++;
				 }
				 
				 x = amRoad.startX;
				 while (x <= amRoad.endX){
					 if (this.am[amRoad.endY][x] == 0)
						 this.am[amRoad.endY][x] = '-';
					 x++;
				 }
			}
		}
	}

	private void drawAsciiMap() {
		ArrayList<String> strRows = convertAsciiMapToRows();
		strRows = trimLeadingEmptyRows(strRows);
		strRows = trimTrailingEmptyRows(strRows);
		System.out.println();
		for (String strRow : strRows) {
			System.out.println(strRow);
		}
		System.out.println();
	}

	private ArrayList<String> convertAsciiMapToRows() {
		ArrayList<String> strRows = new ArrayList<String>();
		for (int row = 0; row < this.rows; row++) {
	        
			StringBuilder builder = new StringBuilder();
	        for (int col = 0; col < this.columns; col++) {
	        	char c = this.am[row][col];
				if (c == 0)
					c = ' ';
				builder.append(c);
			}
			strRows.add(builder.toString());
		}
		return strRows;
	}		


	private ArrayList<String> trimLeadingEmptyRows(ArrayList<String> strRowsIn) {
		ArrayList<String> strRowsOut = new ArrayList<String>();
		boolean isNonEmptuRowEncountered = false;
		for (String strRow : strRowsIn) {
			if (!strRow.trim().isEmpty() || isNonEmptuRowEncountered){
				strRowsOut.add(strRow);
				isNonEmptuRowEncountered = true;
			} 
		}
		return strRowsOut;
	}

	private ArrayList<String> trimTrailingEmptyRows(ArrayList<String> strRows) {
		for (int i = strRows.size() - 1; i > 0; i--) {
			String row = strRows.get(i).trim();
			if (row.isEmpty()){
				strRows.remove(i);
			} else {
				break;
			}
		}
		return strRows;
	}

	
	public static int scaleX(int x) {
		return x * 8;
	}

	public static int scaleY(int y) {
		return y * 3 + 2;
	}

}
