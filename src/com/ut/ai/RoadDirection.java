package com.ut.ai;

public enum RoadDirection {
	North, NorthEast, East, SouthEast, South, SouthWest, West, NorthWest
}
