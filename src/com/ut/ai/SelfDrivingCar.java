package com.ut.ai;

import java.io.IOException;
import java.util.ArrayList;

public class SelfDrivingCar {

	public static void main(String[] args) throws IOException {
		String intersectionsSourceFilePath = args.length > 0 ? args[0] : "";
		String roadsSourceFilePath         = args.length > 1 ? args[1] : "";

		boolean continueDriving = false;
		do {
			Map m = new Map();
			m.readIntersections(intersectionsSourceFilePath);
			m.readRoads(roadsSourceFilePath);
			m.createRoadPoints();
			
	    	UI ui = new UI(m);
	    	ui.showIntroduction();
    	
	    	ui.showMap();
	    	if (ui.askStartAndGoal()){
	    		AStarByRoadPoints a = new AStarByRoadPoints(m);
	    		ArrayList<RoadPoint>shortestPathRP = a.getShortestPath();
	    		ArrayList<Intersection> shortestPath = RoadPoint.flattenToIntersections(shortestPathRP);
	    		try {
		    		ui.showShortestPath(shortestPath);				
				} catch (Exception e) {
					System.out.println("Failed to draw the path.");
					System.out.println("Path is following:");
					System.out.println(shortestPath);
				}
	    	}
	    	continueDriving = ui.askDoYouWantToContinue(); 
    	} while (continueDriving);
   	}
}
