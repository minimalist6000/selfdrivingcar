package com.ut.ai;

public class Road {
	
	private String       name;
	private Intersection start;
	private Intersection end;
	
		
	public Road(String name, Intersection start, Intersection end) {
		this.name = name;
		this.start = start;
		this.end = end;
	}


	public String getName() {
		return name;
	}


	public Intersection getStart() {
		return start;
	}


	public Intersection getEnd() {
		return end;
	}


	@Override
	public String toString() {
		return "Road [name=" + name + ", start=" + start + ", end=" + end + "]";
	}
	
	

}
