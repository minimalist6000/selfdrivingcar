package com.ut.ai;

import java.util.ArrayList;

public class RoadPoint {

	private int x;
	private int y;
	private Road road;
	private boolean isStartPoint;
	private boolean isEndPoint;
	private boolean isIntersection;
	private Intersection intersection;
	private int gScore;  
	private int gScoreTemp;  
	private int hScore;   
	private RoadPoint parent;

	public RoadPoint(int x, int y, Road r, Intersection i) {
		this.x              = x;
		this.y              = y;
		this.gScore         = Integer.MAX_VALUE;
		this.gScoreTemp     = Integer.MAX_VALUE;
		this.road           = r;
		this.intersection   = i;
		this.isIntersection = i != null;
		this.isStartPoint   = this.isIntersection && r.getStart().equals(i);
		this.isEndPoint     = this.isIntersection && r.getEnd().equals(i);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public int getGScore() {
		return gScore;
	}

	public void setGScore(int gScore) {
		this.gScore = gScore;
	}

	public int getGScoreTemp() {
		return gScoreTemp;
	}

	public void setGScoreTemp(int gScoreTemp) {
		this.gScoreTemp = gScoreTemp;
	}

	public int getHScore() {
		return hScore;
	}

	public void setHScore(int hScore) {
		this.hScore = hScore;
	}

	public int getFScore() {
		return gScore + hScore;
	}

	public RoadPoint getParent() {
		return parent;
	}

	public void setParent(RoadPoint parent) {
		this.parent = parent;
	}

	public Road getRoad() {
		return road;
	}

	public boolean isStartPoint() {
		return isStartPoint;
	}

	public boolean isEndPoint() {
		return isEndPoint;
	}

	public boolean isIntersection() {
		return isIntersection;
	}

	public Intersection getIntersection() {
		return intersection;
	}

	@Override
	public String toString() {
		String iName = this.isIntersection ? this.intersection.getName() : "";
		return "RoadPoints [x=" + x + ", y=" + y + ", I=" + iName + "]";
	}

	public static ArrayList<Intersection> flattenToIntersections(ArrayList<RoadPoint> rps) {
		ArrayList<Intersection> is = new ArrayList<Intersection>();
		Intersection parent = null;
		for (RoadPoint rp : rps) {
			if (rp.isIntersection){
				Intersection i = rp.getIntersection();
				if (parent != null) 
					i.setParent(parent);
				is.add(i);
				parent = i;
			}
		}
		
		return is;
	}


	
}
