package com.ut.ai;

import java.io.IOException;
import java.util.ArrayList;

public class Map {
	
	private ArrayList<Intersection> intersections;
	private ArrayList<Road> roads;
	private Intersection start;
	private Intersection goal;
	private ArrayList<RoadPoint> roadPoints;
	private RoadPoint startRoadPoint;
	private RoadPoint goalRoadPoint;
	
	public Map(){
		this.intersections = new ArrayList<Intersection>();    
		this.roads = new ArrayList<Road>();
		this.roadPoints = new ArrayList<RoadPoint>();
	}

	public void readIntersections(String intersectionsSourceFilePath) throws IOException {
		this.intersections = MapCreator.parseIntersections(intersectionsSourceFilePath);
	}
	
	public void readRoads(String roadsSourceFilePath) throws IOException {
		this.roads = MapCreator.parseRoads(roadsSourceFilePath, this.intersections);
	}
	
	public RoadPoint findRoadPointByIntersect(Intersection target) {
		for (RoadPoint rp : this.roadPoints) {
			if (rp.isIntersection() && rp.getIntersection().equals(target)){
				return rp;
			}
		}
		return null;
	}

	public Intersection findIntersectionByName(String iName) {
		for (Intersection intersection : this.intersections){
			if (intersection.getName().toUpperCase().equals(iName.toUpperCase())){
				return intersection;
			}
		}
		return null;
	}
	
	
	public ArrayList<Intersection> getIntersections() {
		return intersections;
	}

	public ArrayList<Road> getRoads() {
		return roads;
	}

	public Intersection getStart() {
		return start;
	}

	public Intersection getGoal() {
		return goal;
	}
	
	public void setGoal(Intersection goal) {
		this.goal = goal;
	}

	public void setStart(Intersection start) {
		this.start = start;
	}
	


	public RoadPoint getStartRoadPoint() {
		return startRoadPoint;
	}

	public void setStartRoadPoint(RoadPoint startRoadPoint) {
		this.startRoadPoint = startRoadPoint;
	}

	public RoadPoint getGoalRoadPoint() {
		return goalRoadPoint;
	}

	public void setGoalRoadPoint(RoadPoint goalRoadPoint) {
		this.goalRoadPoint = goalRoadPoint;
	}

	public RoadPoint getValidRoadPointByCoordinates(RoadPoint current, int x,int y) {
		if (x < 0 || y < 0) return null;		
		
		RoadPoint neighbour = getRoadPointByCoordinates(x, y);
		if (neighbour != null && areRoadPointsLinked(current, neighbour))
			return neighbour;
		return null;
	}

	private RoadPoint getRoadPointByCoordinates(int x, int y) {
		for (RoadPoint rp : this.roadPoints) {
			if (rp.getX() == x && rp.getY() == y){
				return rp;
			}
		}
		return null;
	}

	
	private boolean areRoadPointsLinked(RoadPoint current, RoadPoint neighbour) {
		Road currentRoad  = current.getRoad();
		Road neighbourRoad = neighbour.getRoad(); 
		if (currentRoad.equals(neighbourRoad))
			return true;
		
		if (current.isIntersection() && neighbour.isIntersection()){
			return hasRoadBetweenIntersections(current.getIntersection(), neighbour.getIntersection());
		}
		
		if (currentRoad.getStart().equals(neighbourRoad.getStart()))
			return true;
		
		if (currentRoad.getStart().equals(neighbourRoad.getEnd()))
			return true;
		
		if (currentRoad.getEnd().equals(neighbourRoad.getStart()))
			return true;
		
		if (currentRoad.getEnd().equals(neighbourRoad.getEnd()))
			return true;
		
		return false;
	}
	
	
	public Intersection getValidIntersectionByCoordinates(Intersection current, int x, int y) {
		if (x < 0 || y < 0) return null;		
	
		Intersection neighbour = getIntersectionByCoordinates(x, y);
		if (neighbour != null && hasRoadBetweenIntersections(current, neighbour))
			return neighbour;
		return null;
	}

	private Intersection getIntersectionByCoordinates(int x, int y) {
		for (Intersection intersection : this.intersections) {
			if (intersection.getX() == x && intersection.getY() == y){
				return intersection;
			}
		}
		return null;
	}

	private boolean hasRoadBetweenIntersections(Intersection current, Intersection neighbour) {
		return getRoadBetweenIntersections(current, neighbour) != null;	
	}


	public ArrayList<Road> getRoadsBetweenIntersections(ArrayList<Intersection> intersections) {
		ArrayList<Road> roads = new ArrayList<Road>();
		if (intersections == null) return null;
		
		Intersection parent = null;
		for (Intersection intersection : intersections) {
			if (parent != null && intersection != null) {
				roads.add(getRoadBetweenIntersections(intersection, parent));
			}
			parent = intersection;
		}
		
		return roads;
	}

	public Road getRoadBetweenIntersections(Intersection start, Intersection end){
		for (Road road : this.getRoads()) {
			
			if (road.getStart().equals(start) && road.getEnd().equals(end))
				return road;
			
			if (road.getStart().equals(end) && road.getEnd().equals(start))
				return road;
		}
		
		return null;		
	}
	
	public int getDistanceBetween(Intersection current, Intersection neigbour) {
		return getDistance(current.getX(),current.getY(),neigbour.getX(),neigbour.getY());
	}

	
	private int getDistance(int x1, int y1, int x2, int y2) {
		// Eucledian
		return (int) Math.round(Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2))) + 10;
	}

	public int getEstimateDistanceForRoadPoint(RoadPoint rp) {
		return getEstimatedDistance(rp.getX(),rp.getY(),this.goalRoadPoint.getX(),this.goalRoadPoint.getY());
	}
	
	public int getEstimateDistanceForIntersection(Intersection intersection){
		return getEstimatedDistance(intersection.getX(),intersection.getY(),this.goal.getX(),this.goal.getY());
	}
	
	
	public int getEstimatedDistance(int x1, int y1, int x2, int y2) {
		int dx = x2 - x1;
        int dy = y2 - y1;

        return (dx * dx) + (dy * dy);
	}

	public void createRoadPoints() {
		for (Road r : this.roads) {
			createRoadPointsFromRoad(r);
		}
	}

	private void createRoadPointsFromRoad(Road r) {
		int startX = r.getStart().getX();
		int startY = r.getStart().getY();
		int endX   = r.getEnd().getX();
		int endY   = r.getEnd().getY();
		
		RoadDirection rd = getRoadDirection(startX, startY, endX, endY);
		
		int x = startX;
		int y = startY;
		
		Intersection intersection = getIntersectionByCoordinates(x, y);
		this.roadPoints.add(new RoadPoint(x,y,r,intersection));
		
		while (x != endX && y != endY) {
			switch (rd) {
				case North:          y--; break;				
				case NorthEast: x++; y--; break;					
				case East:      x++;      break;
				case SouthEast: x++; y++; break;					
				case South:          y++; break;
				case SouthWest: x--; y++; break;
				case West:      x--;      break;
				case NorthWest: x--; y--; break;
			}
			intersection = getIntersectionByCoordinates(x, y);
			this.roadPoints.add(new RoadPoint(x,y,r,intersection));
		}	
	}


	private RoadDirection getRoadDirection(int startX, int startY, int endX, int endY) {	
		if (     startX == endX && startY >  endY) return RoadDirection.North;
		else if (startX <  endX && startY >  endY) return RoadDirection.NorthEast;
		else if (startX <  endX && startY == endY) return RoadDirection.East;
		else if (startX <  endX && startY <  endY) return RoadDirection.SouthEast;
		else if (startX == endX && startY <  endY) return RoadDirection.South;
		else if (startX >  endX && startY <  endY) return RoadDirection.SouthWest;
		else if (startX >  endX && startY == endY) return RoadDirection.West;
		else if (startX >  endX && startY >  endY) return RoadDirection.NorthWest;
		
		return null;
	}
	
}
