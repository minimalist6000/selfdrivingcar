package com.ut.ai;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class MapCreator {
	

	public static ArrayList<Intersection> parseIntersections(String intersectionsSourceFilePath) throws IOException {
		if (intersectionsSourceFilePath.isEmpty())
			intersectionsSourceFilePath = getSourceFilePath("ristmikud.txt");
				
		ArrayList<Intersection> intersections = new ArrayList<>();
		BufferedReader br = new BufferedReader(new FileReader(intersectionsSourceFilePath));
		try {            
			String line = br.readLine();
			 
			while (line != null) {
				intersections.add(getIntersectionByLine(line, intersections));
				line = br.readLine();
			}
		} finally {
			br.close();
		}
		return intersections;
	}

	private static Intersection getIntersectionByLine(String line, ArrayList<Intersection> existingIntersections) throws IOException {
		//r1 2 1
		
		String name = parseComponentFromLine(line, 0, "Intersection Name");
		Integer x   = parseIntFromLine(line, 1, "Intersection X coordinate");
		Integer y   = parseIntFromLine(line, 2, "Intersection Y coordinate");

        if (isIntersectionExistingByCoords(existingIntersections,x,y)){
        	throw new IOException("Intersection with given coordinates already exists: " + line);
        }
        
        if (getIntersectionByName(existingIntersections, name) != null){
        	throw new IOException("Intersection with given name already exists: " + line);
        }
        return new Intersection(name, x, y);
	}


	private static boolean isIntersectionExistingByCoords(ArrayList<Intersection> existingIntersections, Integer x, Integer y) {
		for (Intersection i : existingIntersections) {
			if (i.getX() == x && i.getY() == y)
				return true;
		}
		return false;
	}


	public static ArrayList<Road> parseRoads(String roadsSourceFilePath, ArrayList<Intersection> intersections) throws IOException {
		if (roadsSourceFilePath.isEmpty())
			roadsSourceFilePath = getSourceFilePath("teed.txt");
				
		ArrayList<Road> roads = new ArrayList<>();
		BufferedReader br = new BufferedReader(new FileReader(roadsSourceFilePath));
		try {            
			String line = br.readLine();
			 
			while (line != null) {
				roads.add(getRoadByLine(line, intersections, roads));
				line = br.readLine();
			}
		} finally {
			br.close();
		}
		return roads;
	}


	private static String getSourceFilePath(String fileName) {
		File f = new File("./resources/" + fileName);
		
		if (!f.exists()){
			f = new File("../resources/" + fileName);
			if (!f.exists()){
				f = new File(fileName);
			}
		}
		return f.getAbsolutePath();
	}
	
	private static Road getRoadByLine(String line, ArrayList<Intersection> intersections,ArrayList<Road> existingRoads) throws IOException {
		//r1 r2 Liivi
		String startStr = parseComponentFromLine(line, 0, "Road Intersection");
        String endStr   = parseComponentFromLine(line, 1, "Road Intersection");
        String name     = parseComponentFromLine(line, 2, "Road Name");
                
        Intersection start = getIntersectionByName(intersections, startStr);
        Intersection end   = getIntersectionByName(intersections, endStr);
        
        if (start == null){
        	throw new IOException("Intersection " + startStr + " isn't existing.");
        }
        if (end == null){
        	throw new IOException("Intersection " + end      + " isn't existing.");
        }
        
        return new Road(name, start, end);
	}
	
	private static Integer parseIntFromLine(String line, int ix, String type) throws IOException {
		Integer i = null;
		try {
			i = Integer.parseInt(parseComponentFromLine(line, ix, type));
		} catch (Exception e) {
        	throw new IOException("Invalid " + type + ": " + line);
		}
		return i;
	}

	private static String parseComponentFromLine(String line, int ix, String type) throws IOException {
        String[] components = line.split(" ");

        String component = null;
        try {
			component = components[ix].toUpperCase();
		} catch (Exception e) {
        	throw new IOException("Invalid " + type + ": " + line);
		}
        if (component == null || component.isEmpty())
        	throw new IOException("Missing " + type + ": " + line);
       	
        return component;
	}
	
	
	
	private static Intersection getIntersectionByName(ArrayList<Intersection> existingIntersections, String name) {
		for (Intersection intersection : existingIntersections) {
			if (intersection.getName().toUpperCase().equals(name))
				return intersection;
		}
		return null;
	}


}
